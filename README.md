# radioMii

**Mii** 美音

美 _(beautiful)_ 音 _(sound)_

radioMii is a web application to browse and play internet radio streams.

The app pulls the information about the radio streams from the [radio-browser.info](https://www.radio-browser.info/#/) server.

Try the latest version on [radiomii.com](http://radiomii.com/)

The radioMii code is now maintained on https://codeberg.org/haeckse/radioMii

### Used Libraries

- axios
- browser-fs-access
- country-flag-icons
- i18next
- qrcode.react
- react-h5-audio-player
- react-icons
- styled-components
- Zustand

### License

[MIT](https://opensource.org/licenses/mit-license.php)
