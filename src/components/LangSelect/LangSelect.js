import { useState } from 'react';

import { Wrapper, Button, Content, ListItem } from './LangSelect.styles';

import { useTranslation } from 'react-i18next';

export default function Dropdown() {
  const [open, setOpen] = useState(false);

  const { i18n } = useTranslation();

  const lang = i18n.language;

  const languages = [
    { cc: 'bg', lang: 'Български' },
    { cc: 'de', lang: 'Deutsch' },
    { cc: 'en', lang: 'English' },
  ];

  function onItemClicked(value) {
    i18n.changeLanguage(value);
    setOpen(false);
  }

  return (
    <Wrapper
      onMouseLeave={() => {
        setOpen(false);
      }}
    >
      <Button
        onClick={() => {
          setOpen(!open);
        }}
      >
        {languages.map((entry) => {
          if (lang === entry.cc) return entry.lang;
          else return null;
        })}
      </Button>
      {open && (
        <Content>
          {languages.map((entry, id) => {
            return (
              <ListItem
                key={id}
                enabled={lang !== entry.cc}
                onClick={() => {
                  onItemClicked(entry.cc);
                }}
              >
                {entry.lang}
              </ListItem>
            );
          })}
        </Content>
      )}
    </Wrapper>
  );
}
