import styled from 'styled-components';

export const Wrapper = styled.div`
  /* position: relative; */
  width: auto;
  user-select: none;
  z-index: 99;
`;

export const Content = styled.ul`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin: -48px auto;
  width: 136px;
  border: 1px solid var(--miiGreyLight);
  border-radius: 0;
  padding: 1.2em;
  background: ${({ theme }) => theme.color.langsel.bg};
  color: ${({ theme }) => theme.color.langsel.fg};
  font-size: 0.9rem;
  animation: fadeIn 0.25s;

  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }

  li:not(:last-child) {
    margin-bottom: 0.9rem;
  }
`;

export const ListItem = styled.li`
  list-style: none;
  cursor: pointer;
  pointer-events: ${(props) => (props.enabled ? 'auto' : 'none')};
  color: ${(props) =>
    props.enabled
      ? ({ theme }) => theme.color.langsel.fg
      : ({ theme }) => theme.color.langsel.itemDisabled};

  :hover {
    color: var(--miiOrange);
  }
`;

export const Button = styled.button`
  background-color: var(--miiOrange);
  border: none;
  border-radius: 4px;
  cursor: pointer;
  font-size: 0.8rem;
  padding: 0.5rem;
  width: 136px;
  color: white;
  text-decoration: none;
  transition: color 0.2s linear;

  &:hover {
    opacity: 0.9;
  }
`;
