import { useAppStore } from '../../store';
import { IconContext } from 'react-icons';
import {
  IoArrowUp,
  IoArrowDown,
  IoArrowBack,
  IoArrowForward,
  IoCheckmarkSharp,
} from 'react-icons/io5';

import { Wrapper, Content, Button } from './Footer.styles';

export default function Footer({ show, setVirtualKey }) {
  const setStoreFavorite = useAppStore((state) => state.setStoreFavorite);

  return (
    <Wrapper show={show}>
      <Content>
        <IconContext.Provider
          value={{ size: '24px', style: { verticalAlign: '-10%' } }}
        >
          <Button onClick={() => setVirtualKey('ArrowUp')}>
            <IoArrowUp />
          </Button>
          <Button onClick={() => setVirtualKey('ArrowDown')}>
            <IoArrowDown />
          </Button>
          <Button onClick={() => setVirtualKey('ArrowLeft')}>
            <IoArrowBack />
          </Button>
          <Button onClick={() => setVirtualKey('ArrowRight')}>
            <IoArrowForward />
          </Button>
          <Button onClick={() => setStoreFavorite({ move: false })}>
            <IoCheckmarkSharp />
          </Button>
        </IconContext.Provider>
      </Content>
    </Wrapper>
  );
}
