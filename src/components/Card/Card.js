import { useState } from 'react';
import PropTypes from 'prop-types';
import { useAppStore, useSettingsStore } from '../../store';
import { getItemFromStorage } from '../../utils/localStorage';
import { trimString, removeMarkdown, getPlainName } from '../../utils/string';
import Flags from 'country-flag-icons/react/3x2';
import RadioSvg from '../../images/radio.svg';
import RadioPng from '../../images/radio.png';
import { IoThumbsUpSharp, IoPlaySharp } from 'react-icons/io5';
import { Dropdown, ToggleHeart, Tooltip } from '../';
import { sendStationClick } from '../../RadioBrowserApi';

import {
  Wrapper,
  Content,
  IconStation,
  IconPlay,
  Footer,
  Label,
} from './Card.styles';

let formatter = Intl.NumberFormat('en', { notation: 'compact' });

export default function Card({ ...props }) {
  const [showPlayer, setShowPlayer] = useState(false);
  const [showDots, setShowDots] = useState(false);
  const [showImg, setShowImg] = useState(true);
  const [wiggleCard, setWiggleCard] = useState(false);

  const setActiveStation = useAppStore((state) => state.setActiveStation);
  const VIEW = useAppStore((state) => state.VIEW);
  const viewMode = useAppStore((state) => state.viewMode);
  const compactCard = useSettingsStore((state) => state.compactCard);
  const storeFavorite = useAppStore((state) => state.storeFavorite);
  const setStoreFavorite = useAppStore((state) => state.setStoreFavorite);
  const system = useAppStore((state) => state.system);

  if (!storeFavorite.move && wiggleCard) {
    setWiggleCard(false);
  }

  const Flag = Flags[props.station.countrycode];

  const tags = props.station.tags
    .toString()
    .split(',')
    .filter((tag) => tag.length > 0 && tag.length < 16);

  const name = removeMarkdown(props.station.name);

  function startAudio(url) {
    props.player.current.audio.current.src = url;

    sendStationClick(props.station.stationuuid);

    setActiveStation({
      name: name,
      url: url,
      uuid: props.station.stationuuid,
      image: showImg ? props.station.favicon : RadioPng,
    });
  }

  function getEnabledItems() {
    var hasHomepage = 0;
    var hasMoreFavorites = 0;

    let array = getItemFromStorage('favorites');

    props.station.homepage ? (hasHomepage = 4) : (hasHomepage = 0);

    if (array) {
      if (array.results.length > 1) {
        hasMoreFavorites = 2;
      }
    }

    return hasHomepage + hasMoreFavorites;
  }

  return (
    <>
      <Wrapper
        wiggle={wiggleCard}
        compactCard={compactCard}
        move={storeFavorite.move}
        onMouseEnter={() => {
          !system.isTouch && setShowPlayer(true);
          setShowDots(true);
        }}
        onMouseLeave={() => {
          !system.isTouch && setShowPlayer(false);
          setShowDots(false);
        }}
      >
        <Content compactCard={compactCard}>
          {viewMode === VIEW.FAVORITES &&
            !compactCard &&
            !storeFavorite.move && (
              <Dropdown
                id={props.id}
                showDots={showDots}
                setWiggleCard={setWiggleCard}
                enabledItems={getEnabledItems()}
                station={props.station}
              />
            )}
          {showImg ? (
            <IconStation
              compactCard={compactCard}
              src={props.station.favicon}
              alt='station-image'
              onError={() => {
                setShowImg(false);
              }}
            />
          ) : (
            <IconStation src={RadioSvg} alt='station-image' />
          )}
          {trimString(getPlainName(name), 80)}
          {viewMode !== VIEW.FAVORITES && (
            <ToggleHeart
              checked={props.favorite}
              cbClicked={(checked) => {
                setStoreFavorite({
                  move: false,
                  station: props.station,
                  isFavorite: checked,
                });
              }}
            />
          )}
          {!compactCard && (
            <Footer compactCard={compactCard}>
              <Label color='cornflowerblue'>
                <IoThumbsUpSharp />
                {'\u00A0'}
                {formatter.format(props.station.votes)}
              </Label>
              <Label color='#fa7d18'>
                <IoPlaySharp />
                {formatter.format(props.station.clickcount)}
              </Label>
              {Flag ? <Flag className='flag' /> : ''}
              {props.station.countrycode && (
                <Tooltip
                  content={props.station.country}
                  direction='bottom'
                  bgColor='#9e49bd'
                  margin='32px'
                  delay='0.2s'
                >
                  <Label color='#9e49bd'>{props.station.countrycode}</Label>
                </Tooltip>
              )}
              {props.station.codec && (
                <Label color='mediumseagreen'>{props.station.codec}</Label>
              )}
              {props.station.bitrate ? (
                <Label color='#bc3737'>{props.station.bitrate + ' kB/s'}</Label>
              ) : (
                ''
              )}
              {tags.map((tag, idx) => {
                return <Label key={idx}>{tag}</Label>;
              })}
            </Footer>
          )}
          {(showPlayer || system.isTouch) && !storeFavorite.move && (
            <IconPlay
              isTouch={system.isTouch}
              compactCard={compactCard}
              onClick={() => {
                startAudio(props.station.url_resolved);
              }}
            >
              {!system.isTouch && <IoPlaySharp />}
            </IconPlay>
          )}
        </Content>
      </Wrapper>
    </>
  );
}

Card.propTypes = {
  id: PropTypes.number,
  player: PropTypes.object,
  image: PropTypes.string,
  url: PropTypes.string,
  votes: PropTypes.number,
  countryCode: PropTypes.string,
  bitrate: PropTypes.number,
  name: PropTypes.string,
  tags: PropTypes.array,
};
