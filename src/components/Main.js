import { useState, useRef } from 'react';
import { useAppStore } from '../store';
import { getItemFromStorage } from '../utils/localStorage';
import { useHotkeys } from '../hooks/useHotkeys';
import { useFetchStations } from '../hooks/useFetchStations';
import { useStoreFavorites } from '../hooks/useStoreFavorites';

import { Card, EmptyState, Grid, Header, SearchBar, Footer } from './';

const Main = ({ ...props }) => {
  const { stations, loading } = useFetchStations();

  let favorites = getItemFromStorage('favorites');

  const [virtualKey, setVirtualKey] = useState(false);

  const VIEW = useAppStore((state) => state.VIEW);
  const viewMode = useAppStore((state) => state.viewMode);
  const msgDialog = useAppStore((state) => state.msgDialog);
  const showDialog = useAppStore((state) => state.showDialog);

  const storeFavorite = useAppStore((state) => state.storeFavorite);

  const system = useAppStore((state) => state.system);

  var modalActive = msgDialog.show;

  for (const item in showDialog) {
    modalActive |= showDialog[item];
  }

  const player = useRef(null);
  const cardItemsRef = useRef(null);

  const { setKey } = useStoreFavorites(storeFavorite);

  const getCardRefsMap = () => {
    if (!cardItemsRef.current) {
      cardItemsRef.current = new Map();
    }

    return cardItemsRef.current;
  };

  const scrollToCard = (itemId) => {
    const map = getCardRefsMap();
    const node = map.get(itemId);

    node.scrollIntoView({
      behavior: 'smooth',
      block: 'center',
    });
  };

  useHotkeys(setKey, virtualKey, setVirtualKey, player, scrollToCard);

  const showEmpty =
    viewMode === VIEW.EMPTY_STATE_FAVS ||
    viewMode === VIEW.EMPTY_STATE_RESULTS ||
    viewMode === VIEW.EMPTY_STATE_ERROR;

  return (
    <>
      <Header player={player} {...props} />
      <SearchBar />
      {showEmpty && <EmptyState />}
      <Grid loading={loading} modalActive={modalActive}>
        {stations &&
          stations.results.map((entry, idx) => {
            return (
              <div
                key={entry.stationuuid}
                ref={(node) => {
                  const map = getCardRefsMap();
                  node
                    ? map.set(entry.stationuuid, node)
                    : map.delete(entry.stationuuid);
                }}
              >
                <Card
                  id={idx}
                  player={player}
                  station={entry}
                  favorite={
                    favorites
                      ? favorites.results.some(
                          (e) => e.stationuuid === entry.stationuuid
                        )
                      : false
                  }
                />
              </div>
            );
          })}
      </Grid>
      <Footer
        show={storeFavorite.move && system.isTouch}
        setVirtualKey={setVirtualKey}
      />
    </>
  );
};

export default Main;
