import { useState } from 'react';
import { useAppStore } from '../../store';

import AudioPlayer from 'react-h5-audio-player';
import { Burger, Menu, Infobox } from '../';
import 'react-h5-audio-player/lib/styles.css';
import './player.css';
import YordaLogo from '../../images/yorda-logo.svg';
import { Wrapper, Content, LogoImg, InfoBoxWrap } from './Header.styles';
import { useFetchMetaData } from '../../hooks/useFetchMetaData';

export default function Header({ ...props }) {
  const [loadStation, setLoadStation] = useState();
  const [showSongInfo, setShowSongInfo] = useState();
  const [open, setOpen] = useState(false);

  const activeStation = useAppStore((state) => state.activeStation);
  const setShowDialog = useAppStore((state) => state.setShowDialog);

  const { metaData, setGetMetaActive, setUrl } = useFetchMetaData();

  return (
    <Wrapper>
      <Content>
        <Burger open={open} setOpen={setOpen} />
        <Menu open={open} setOpen={setOpen} id={'main-menu'} />
        <LogoImg
          className='logo'
          src={YordaLogo}
          alt='yorda-logo'
          onClick={() => {
            setShowDialog({ info: true });
          }}
        />
        <InfoBoxWrap className='info'>
          <Infobox
            metaData={metaData}
            loadStation={loadStation}
            showSongInfo={showSongInfo}
            {...props}
          />
        </InfoBoxWrap>
        <AudioPlayer
          volume='0.7'
          autoPlay={true}
          showJumpControls={false}
          showFilledVolume={true}
          layout='stacked'
          customProgressBarSection={[]}
          customControlsSection={['MAIN_CONTROLS', 'VOLUME_CONTROLS']}
          autoPlayAfterSrcChange={false}
          hasDefaultKeyBindings={false}
          onLoadStart={(e) => {
            setLoadStation(true);
            setShowSongInfo(false);
            setGetMetaActive(false);
            setUrl(activeStation.url);
          }}
          onPlaying={(e) => {
            setLoadStation(false);
            setShowSongInfo(true);
            setGetMetaActive(true);
          }}
          onPause={(e) => {
            setShowSongInfo(false);
            setGetMetaActive(false);
          }}
          onError={(e) => console.log('Audioplayer: onError', e)}
          ref={props.player}
        />
      </Content>
    </Wrapper>
  );
}
