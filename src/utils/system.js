export function isDevelop() {
  if (process.env.NODE_ENV === 'development') {
    return true;
  }

  return false;
}

export function isMobile() {
  let userAgent = navigator.userAgent || navigator.vendor || window.opera;

  if (
    /windows phone/i.test(userAgent) ||
    /android/i.test(userAgent) ||
    (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream)
  ) {
    return true;
  }

  return false;
}

export function isTouch() {
  if ('ontouchstart' in document.documentElement) {
    return true;
  }

  return false;
}
