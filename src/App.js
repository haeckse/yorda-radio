import { useEffect } from 'react';
import { useAppStore, useSettingsStore } from './store';
import { ThemeProvider } from 'styled-components';
import { lightTheme, darkTheme } from './theme';
import { GlobalStyle } from './GlobalStyle';
import Main from './components/Main';
import Dialogs from './components/Dialogs/Dialogs';
import { isDevelop, isTouch } from './utils/system';

export default function App() {
  const light = useSettingsStore((state) => state.light);
  const system = useAppStore((state) => state.system);
  const setSystem = useAppStore((state) => state.setSystem);
  const setShowTooltips = useSettingsStore((state) => state.setShowTooltips);
  const setStationId = useAppStore((state) => state.setStationId);
  const VIEW = useAppStore((state) => state.VIEW);
  const setViewMode = useAppStore((state) => state.setViewMode);
  const error = useAppStore((state) => state.error);
  const setCountries = useAppStore((state) => state.setCountries);

  /* eslint-disable */
  useEffect(() => {
    setSystem({
      isTouch: isTouch(),
      isDevelop: isDevelop(),
    });

    if (system.isTouch) setShowTooltips(false);
  }, []);

  useEffect(() => {
    setCountries();
  }, []);

  useEffect(() => {
    // search for URI stationUUID param like:
    // .../?id=963ccae5-0601-11e8-ae97-52543be04c81

    const query = new URLSearchParams(window.location.search);
    const uuid = query.get('id');

    if (uuid) {
      setStationId(uuid);

      const url = new URL(window.location);
      url.searchParams.delete('id');
      window.history.replaceState(null, null, url);
    }
  }, []);

  useEffect(() => {
    if (error.set) {
      setViewMode(VIEW.EMPTY_STATE_ERROR);
    }
  }, [error]);
  /* eslint-enable */

  return (
    <div className='App'>
      <ThemeProvider theme={light ? lightTheme : darkTheme}>
        <GlobalStyle />
        <Main />
        <Dialogs />
      </ThemeProvider>
    </div>
  );
}
